/**
 * The MIT License (MIT)
 *
 * Author: Lakshantha Dissanayake (lakshanthad@seeed.cc)
 *
 * Copyright (C) 2020  Seeed Technology Co.,Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


//needed header files from micropython
#include "py/mphal.h"
#include "py/nlr.h"
#include "py/objtype.h"
#include "py/runtime.h"
#include "py/obj.h"
#include "shared-bindings/util.h"

//include previously defined functions from wrapper
void common_hal_lcdbacklight_construct(abstract_module_t *self);
void common_hal_lcdbacklight_deinit(abstract_module_t *self);
void common_hal_lcdbacklight_turn_off(abstract_module_t *self);
void common_hal_lcdbacklight_turn_on(abstract_module_t *self);

extern const mp_obj_type_t lcdbacklight_type; //declaration of initial module type 

//initialize module 
m_generic_make(lcdbacklight) {
    abstract_module_t * self = new_abstruct_module(type);
    mp_arg_check_num(n_args, n_kw, 0, 0, false);
    common_hal_lcdbacklight_construct(self);
    return self;
}

MP_DEFINE_CONST_FUN_OBJ_1(lcdbacklight_turn_off_obj, common_hal_lcdbacklight_turn_off);
MP_DEFINE_CONST_FUN_OBJ_1(lcdbacklight_turn_on_obj, common_hal_lcdbacklight_turn_on);

//look-up table 
const mp_rom_map_elem_t lcdbacklight_locals_dict_table[] = {
    //instance methods
    {MP_ROM_QSTR(MP_QSTR_deinit), MP_ROM_PTR(&lcdbacklight_deinit_obj)},
    {MP_ROM_QSTR(MP_QSTR___enter__), MP_ROM_PTR(&default___enter___obj)},
    {MP_ROM_QSTR(MP_QSTR___exit__), MP_ROM_PTR(&lcdbacklight_obj___exit___obj)},
    {MP_ROM_QSTR(MP_QSTR_turnOff), MP_ROM_PTR(&lcdbacklight_turn_off_obj)},
    {MP_ROM_QSTR(MP_QSTR_turnOn), MP_ROM_PTR(&lcdbacklight_turn_on_obj)},
};

MP_DEFINE_CONST_DICT(lcdbacklight_locals_dict, lcdbacklight_locals_dict_table);

//initial module type 
const mp_obj_type_t lcdbacklight_type = {
    {&mp_type_type},
    .name = MP_QSTR_lcdbacklight,
    .make_new = lcdbacklight_make_new,
    .locals_dict = (mp_obj_t)&lcdbacklight_locals_dict,
};