# Wio Terminal Ardupy Backlight

## Introduction

This is the ArduPy library for controlling the backlight of the LCD on the Wio Terminal. 
This library has been influenced by seeed WT_LCDBacklight library (https://github.com/Seeed-Studio/seeed-ardupy-WTbacklight.git).


## How to bind with ArduPy

NOTE: Please ensure you have updated the install.py file in the aip package as detailed below as it is required to install from bitbucket

- Install [AIP](https://github.com/Seeed-Studio/ardupy-aip)
- Build firmware with Seeed ArduPy DHT
```shell
aip install https://bitbucket.org/argusglobal/heimdall-wio-backlight/get/dev.zip
aip build
```
- Flash new firmware to you ArduPy board
```shell
aip flash
#aip flash [path of the new firmware]
```

## Usage

```python
# include libraries  
from arduino import lcdbacklight
from machine import LCD
import time

lcd = LCD() # initialize LCD
backlight = lcdbacklight() # initialize backlight

def main(): # main function 
    lcd.fillScreen(lcd.color.GREEN) # fill background 
    
    time.sleep(3)
    
    backlight.turnOff()
    
    time.sleep(3)
    
    lcd.fillScreen(lcd.color.WHITE) # fill background
    
    backlight.turnOn()
    
    time.sleep(3)
    
    backlight.turnOff()
        
  
if __name__ == "__main__": # check whether this is run from main.py
    main() # execute function
```

## API Reference

- **turnOff** : Turn Off the LCD Backlight
```python
 backlight.turnOff()
```

- **turnOn** : Turn On the LCD Backlight
```python
 backlight.turnOn()
```

## AIP install.py changes

Please replace the with get_archive_url in install.py with the following:

```python
    def get_archive_url(self, options, package):
        ## parse the url
        if "bitbucket.org" in package:
            package_url = package
        else: 
            package = package.replace('.git', '')
            package_parse = urlparse(package)
            
            if package_parse.scheme != '':
                scheme = package_parse.scheme   # get scheme
            else:
                scheme = options.scheme
            if package_parse.netloc != '':
                netloc = package_parse.netloc   # get net location
            else:
                netloc = options.netloc

            if "/archive/" in package_parse.path and '.zip' in package_parse.path:  
                path = package_parse.path   # get the path
            else:
                branch = scheme + '://' + netloc + '/' + package_parse.path + '/tree/' + 'master'
                re =  requests.head(branch)
                if re.status_code != 404:
                    path = package_parse.path + '/archive/master.zip'
                else:
                    path = package_parse.path + '/archive/main.zip'
                
            package_url = scheme + '://' + netloc + '/' + path  # form path
            
        return package_url
```

